package com.message.admin.mot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.message.admin.comm.controller.BaseRestController;
import com.message.admin.mot.pojo.MotEvent;
import com.message.admin.mot.service.MotEventService;
import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.system.comm.model.Orderby;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_event的Controller
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@RestController
public class MotEventController extends BaseRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MotEventController.class);

	@Autowired
	private MotEventService motEventService;
	
	@RequestMapping(name = "motEvent-获取详细信息", value = "/motEvent/get")
	@ApiInfo(params = {
			@ApiParam(name="meId", code="meId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			
	})
	public ResponseFrame get(String meId) {
		try {
			ResponseFrame frame = new ResponseFrame();
			frame.setBody(motEventService.get(meId));
			frame.setCode(ResponseCode.SUCC.getCode());
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motEvent-新增或修改", value = "/motEvent/saveOrUpdate")
	@ApiInfo(params = {
			@ApiParam(name="编号", code="meId", value=""),
			@ApiParam(name="来源系统编码", code="sysNo", value=""),
			@ApiParam(name="名称", code="name", value=""),
			@ApiParam(name="备注", code="remark", value=""),
			@ApiParam(name="调度加密方式[10无、20md5(clientId+时间戳+token)]", code="reqEncrypt", value=""),
			@ApiParam(name="过滤插件", code="filterPlug", value=""),
			@ApiParam(name="创建人", code="createUserId", value=""),
			@ApiParam(name="创建时间", code="createTime", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame saveOrUpdate(MotEvent motEvent) {
		try {
			ResponseFrame frame = motEventService.saveOrUpdate(motEvent);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motEvent-分页查询信息", value = "/motEvent/pageQuery")
	@ApiInfo(params = {
			@ApiParam(name="页面", code="page", value="1"),
			@ApiParam(name="每页大小", code="size", value="10"),
			@ApiParam(name="排序[{\"property\": \"createTime\", \"type\":\"desc\", \"order\":1}]", code="orderby", value="", required=false),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			@ApiRes(name="当前页码", code="page", pCode="body", clazz=Integer.class, value="1"),
			@ApiRes(name="每页大小", code="size", pCode="body", clazz=Integer.class, value="10"),
			@ApiRes(name="总页数", code="totalPage", pCode="body", clazz=Integer.class, value="5"),
			@ApiRes(name="总记录数", code="total", pCode="body", clazz=Integer.class, value="36"),
			@ApiRes(name="数据集合", code="rows", pCode="body", clazz=List.class, value=""),
			@ApiRes(name="编号", code="meId", pCode="rows", value=""),
			@ApiRes(name="来源系统编码", code="sysNo", pCode="rows", value=""),
			@ApiRes(name="名称", code="name", pCode="rows", value=""),
			@ApiRes(name="备注", code="remark", pCode="rows", value=""),
			@ApiRes(name="调度加密方式[10无、20md5(clientId+时间戳+token)]", code="reqEncrypt", pCode="rows", value=""),
			@ApiRes(name="过滤插件", code="filterPlug", pCode="rows", value=""),
			@ApiRes(name="创建人", code="createUserId", pCode="rows", value=""),
			@ApiRes(name="创建时间", code="createTime", pCode="rows", value=""),
	})
	public ResponseFrame pageQuery(MotEvent motEvent, String orderby) {
		try {
			if(FrameStringUtil.isNotEmpty(orderby)) {
				List<Orderby> orderbys = FrameJsonUtil.toList(orderby, Orderby.class);
				motEvent.setOrderbys(orderbys);
			}
			ResponseFrame frame = motEventService.pageQuery(motEvent);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motEvent-根据主键删除", value = "/motEvent/delete")
	@ApiInfo(params = {
			@ApiParam(name="meId", code="meId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame delete(String meId) {
		try {
			ResponseFrame frame = motEventService.delete(meId);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}
}
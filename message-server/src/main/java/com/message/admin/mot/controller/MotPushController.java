package com.message.admin.mot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.message.admin.comm.controller.BaseRestController;
import com.message.admin.mot.pojo.MotPush;
import com.message.admin.mot.service.MotPushService;
import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.system.comm.model.Orderby;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_push的Controller
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@RestController
public class MotPushController extends BaseRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MotPushController.class);

	@Autowired
	private MotPushService motPushService;
	
	@RequestMapping(name = "motPush-获取详细信息", value = "/motPush/get")
	@ApiInfo(params = {
			@ApiParam(name="mpId", code="mpId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			
	})
	public ResponseFrame get(String mpId) {
		try {
			ResponseFrame frame = new ResponseFrame();
			frame.setBody(motPushService.get(mpId));
			frame.setCode(ResponseCode.SUCC.getCode());
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motPush-新增或修改", value = "/motPush/save")
	@ApiInfo(params = {
			@ApiParam(name="来源系统", code="sysNo", value=""),
			@ApiParam(name="事件编号", code="meId", value=""),
			@ApiParam(name="标题", code="title", value=""),
			@ApiParam(name="内容", code="content", value=""),
			@ApiParam(name="接收人[多个;分隔，0代表所有订阅人]", code="receUserIds", value=""),
			@ApiParam(name="邮件内容", code="emailContent", value=""),
			@ApiParam(name="邮件附件[多个;分隔]", code="emailFiles", value=""),
			@ApiParam(name="短信内容", code="smsContent", value=""),
			@ApiParam(name="接收邮箱[直接接收]", code="receEmail", value=""),
			@ApiParam(name="接收短信手机号[直接接收]", code="receSms", value=""),
			@ApiParam(name="过滤参数[{\"type\":\"\",\"field1\":\"\",\"field2\":\"\"}]", code="filterString", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame save(MotPush motPush) {
		try {
			ResponseFrame frame = motPushService.save(motPush);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motPush-分页查询信息", value = "/motPush/pageQuery")
	@ApiInfo(params = {
			@ApiParam(name="页面", code="page", value="1"),
			@ApiParam(name="每页大小", code="size", value="10"),
			@ApiParam(name="排序[{\"property\": \"createTime\", \"type\":\"desc\", \"order\":1}]", code="orderby", value="", required=false),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			@ApiRes(name="当前页码", code="page", pCode="body", clazz=Integer.class, value="1"),
			@ApiRes(name="每页大小", code="size", pCode="body", clazz=Integer.class, value="10"),
			@ApiRes(name="总页数", code="totalPage", pCode="body", clazz=Integer.class, value="5"),
			@ApiRes(name="总记录数", code="total", pCode="body", clazz=Integer.class, value="36"),
			@ApiRes(name="数据集合", code="rows", pCode="body", clazz=List.class, value=""),
			@ApiRes(name="编号", code="mpId", pCode="rows", value=""),
			@ApiRes(name="来源系统", code="sysNo", pCode="rows", value=""),
			@ApiRes(name="事件编号", code="meId", pCode="rows", value=""),
			@ApiRes(name="标题", code="title", pCode="rows", value=""),
			@ApiRes(name="内容", code="content", pCode="rows", value=""),
			@ApiRes(name="接收人[多个;分隔，0代表所有订阅人]", code="receUserIds", pCode="rows", value=""),
			@ApiRes(name="邮件内容", code="emailContent", pCode="rows", value=""),
			@ApiRes(name="邮件附件[多个;分隔]", code="emailFiles", pCode="rows", value=""),
			@ApiRes(name="短信内容", code="smsContent", pCode="rows", value=""),
			@ApiRes(name="创建时间", code="createTime", pCode="rows", value=""),
	})
	public ResponseFrame pageQuery(MotPush motPush, String orderby) {
		try {
			if(FrameStringUtil.isNotEmpty(orderby)) {
				List<Orderby> orderbys = FrameJsonUtil.toList(orderby, Orderby.class);
				motPush.setOrderbys(orderbys);
			}
			ResponseFrame frame = motPushService.pageQuery(motPush);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motPush-根据主键删除", value = "/motPush/delete")
	@ApiInfo(params = {
			@ApiParam(name="mpId", code="mpId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame delete(String mpId) {
		try {
			ResponseFrame frame = motPushService.delete(mpId);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}
}
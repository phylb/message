package com.message.admin.mot.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.system.comm.model.KvEntity;

/**
 * 调度加密方式[10无、20md5(clientId+时间戳+token)]
 * @author yuejing
 * @date 2016年5月16日 下午5:52:03
 * @version V1.0.0
 */
public enum MotEventReqEncrypt {
	// 无加密
	NOTH		(10, "无加密"),
	// md5(clientId+时间戳+token)
	MD5_CTT		(20, "md5(clientId+时间戳+token)"),
	;

	public static final String KEY = "mot_event_reqEncrypt";
	
	private Integer code;
	private String name;
	private static List<KvEntity> list = new ArrayList<KvEntity>();
	private static Map<Integer, String> map = new HashMap<Integer, String>();

	private MotEventReqEncrypt(Integer code, String name) {
		this.code = code;
		this.name = name;
	}
	
	static {
		EnumSet<MotEventReqEncrypt> set = EnumSet.allOf(MotEventReqEncrypt.class);
		for(MotEventReqEncrypt e : set){
			map.put(e.getCode(), e.getName());
			list.add(new KvEntity(String.valueOf(e.getCode()), e.getName()));
		}
	}

	/**
	 * 根据Code获取对应的汉字
	 * @param code
	 * @return
	 */
	public static String getText(Integer code) {
		return map.get(code);
	}
	
	/**
	 * 获取集合
	 * @return
	 */
	public static List<KvEntity> getList() {
		return list;
	}

	public Integer getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
}

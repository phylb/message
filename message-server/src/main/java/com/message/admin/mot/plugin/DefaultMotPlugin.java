package com.message.admin.mot.plugin;

import java.util.ArrayList;
import java.util.List;

import com.message.admin.mot.plugin.core.BaseMotPlugin;
import com.message.admin.mot.pojo.MotPush;
import com.message.admin.mot.pojo.MotSub;
import com.message.admin.mot.service.MotSubService;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseFrame;

/**
 * 默认的插件<br>
 * 	不处理参数，推送的接收人和订阅人一致时才能接收消息
 * @author yuejing
 * @date 2018年12月19日 上午10:00:22
 */
public class DefaultMotPlugin extends BaseMotPlugin {

	@Override
	public ResponseFrame trigger(MotPush push) {
		ResponseFrame frame = new ResponseFrame();
		MotSubService motSubService = FrameSpringBeanUtil.getBean(MotSubService.class);
		List<MotSub> list = new ArrayList<MotSub>();
		if ("0".equals(push.getReceUserIds())) {
			// 所有人接收
			list.addAll(motSubService.findByMeId(push.getMeId()));
		} else {
			// 指定了接收人
			List<String> receUserIds = FrameStringUtil.toArray(push.getReceUserIds(), ";");
			for (String uid : receUserIds) {
				MotSub ms = motSubService.getByMeIdSubuid(push.getMeId(), uid);
				if (ms != null) {
					list.add(ms);
				}
			}
		}
		// 发送消息
		sendMsg(push, list);
		frame.setSucc();
		return frame;
	}

}
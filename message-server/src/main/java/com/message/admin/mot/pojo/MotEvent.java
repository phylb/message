package com.message.admin.mot.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * mot_event实体
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Alias("motEvent")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MotEvent extends BaseEntity implements Serializable {
	// 编号
	private String meId;
	// 来源系统编码
	private String sysNo;
	// 名称
	private String name;
	// 备注
	private String remark;
	// 调度加密方式[10无、20md5(clientId+时间戳+token)]
	private Integer reqEncrypt;
	// 过滤插件
	private String filterPlug;
	// 创建人
	private String createUserId;
	// 创建时间
	private Date createTime;
	
	//================================= 扩展属性
	// 系统名称
	private String sysName;
	// 调度加密方式名称
	private String reqEncryptName;
	// 过滤插件名称
	private String filterPlugName;
	
	public String getMeId() {
		return meId;
	}
	public void setMeId(String meId) {
		this.meId = meId;
	}
	
	public String getSysNo() {
		return sysNo;
	}
	public void setSysNo(String sysNo) {
		this.sysNo = sysNo;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Integer getReqEncrypt() {
		return reqEncrypt;
	}
	public void setReqEncrypt(Integer reqEncrypt) {
		this.reqEncrypt = reqEncrypt;
	}
	
	public String getFilterPlug() {
		return filterPlug;
	}
	public void setFilterPlug(String filterPlug) {
		this.filterPlug = filterPlug;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getSysName() {
		return sysName;
	}
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}
	public String getReqEncryptName() {
		return reqEncryptName;
	}
	public void setReqEncryptName(String reqEncryptName) {
		this.reqEncryptName = reqEncryptName;
	}
	public String getFilterPlugName() {
		return filterPlugName;
	}
	public void setFilterPlugName(String filterPlugName) {
		this.filterPlugName = filterPlugName;
	}
}
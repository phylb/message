package com.message.admin.mot.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * mot_push实体
 * @author yuejing
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Alias("motPush")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MotPush extends BaseEntity implements Serializable {
	// 编号
	private String mpId;
	// 来源系统
	private String sysNo;
	// 事件编号
	private String meId;
	// 标题
	private String title;
	// 内容
	private String content;
	// 接收人[多个;分隔，0代表所有订阅人]
	private String receUserIds;
	// 邮件内容
	private String emailContent;
	// 邮件附件[多个;分隔]
	private String emailFiles;
	// 短信内容
	private String smsContent;
	// 创建时间
	private Date createTime;
	
	//================================= 扩展属性
	// 系统名称
	private String sysName;
	// 事件名称
	private String eventName;
	// 过滤条件集合字符串
	private String filterString;
	// 过滤条件
	private List<MotPushFilter> filters;
	// 实际接收人列表
	private List<MotPushRece> reces;

	// 邮件通知号码{receUserId:"t@qq.com;b@qq.com"}
	private String receEmail;
	// 短信通知号码{receUserId:"13309874567;13398761234"}
	private String receSms;
	
	public String getMpId() {
		return mpId;
	}
	public void setMpId(String mpId) {
		this.mpId = mpId;
	}
	
	public String getSysNo() {
		return sysNo;
	}
	public void setSysNo(String sysNo) {
		this.sysNo = sysNo;
	}
	
	public String getMeId() {
		return meId;
	}
	public void setMeId(String meId) {
		this.meId = meId;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getReceUserIds() {
		return receUserIds;
	}
	public void setReceUserIds(String receUserIds) {
		this.receUserIds = receUserIds;
	}
	
	public String getEmailContent() {
		return emailContent;
	}
	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}
	
	public String getEmailFiles() {
		return emailFiles;
	}
	public void setEmailFiles(String emailFiles) {
		this.emailFiles = emailFiles;
	}
	
	public String getSmsContent() {
		return smsContent;
	}
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getSysName() {
		return sysName;
	}
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}
	public String getFilterString() {
		return filterString;
	}
	public void setFilterString(String filterString) {
		this.filterString = filterString;
	}
	public List<MotPushFilter> getFilters() {
		return filters;
	}
	public void setFilters(List<MotPushFilter> filters) {
		this.filters = filters;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public List<MotPushRece> getReces() {
		return reces;
	}
	public void setReces(List<MotPushRece> reces) {
		this.reces = reces;
	}
	public String getReceEmail() {
		return receEmail;
	}
	public void setReceEmail(String receEmail) {
		this.receEmail = receEmail;
	}
	public String getReceSms() {
		return receSms;
	}
	public void setReceSms(String receSms) {
		this.receSms = receSms;
	}
}
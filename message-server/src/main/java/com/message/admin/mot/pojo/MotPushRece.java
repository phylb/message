package com.message.admin.mot.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * mot_push_rece实体
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Alias("motPushRece")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MotPushRece extends BaseEntity implements Serializable {
	// 推送事件编号
	private String mpId;
	// 事件订阅人编号
	private String msId;
	// 消息编号
	private String msgId;
	// 创建时间
	private Date createTime;
	
	//========================== 扩展属性
	// 接收人
	private String receUserId;
	// 接收邮箱[多个;分隔]
	private String receEmail;
	// 接收短信通知号码[多个;分隔]
	private String receSms;
	
	public MotPushRece() {
		super();
	}
	public MotPushRece(String mpId, String msId, String msgId) {
		super();
		this.mpId = mpId;
		this.msId = msId;
		this.msgId = msgId;
	}
	public String getMpId() {
		return mpId;
	}
	public void setMpId(String mpId) {
		this.mpId = mpId;
	}
	
	public String getMsId() {
		return msId;
	}
	public void setMsId(String msId) {
		this.msId = msId;
	}
	
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getReceUserId() {
		return receUserId;
	}
	public void setReceUserId(String receUserId) {
		this.receUserId = receUserId;
	}
	public String getReceEmail() {
		return receEmail;
	}
	public void setReceEmail(String receEmail) {
		this.receEmail = receEmail;
	}
	public String getReceSms() {
		return receSms;
	}
	public void setReceSms(String receSms) {
		this.receSms = receSms;
	}
}
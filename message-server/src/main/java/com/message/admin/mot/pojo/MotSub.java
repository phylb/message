package com.message.admin.mot.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * mot_sub实体
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Alias("motSub")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MotSub extends BaseEntity implements Serializable {
	// 编号
	private String msId;
	// 来源系统编号
	private String sysNo;
	// 事件编号
	private String meId;
	// 订阅人
	private String subUserId;
	// 邮件通知号码[多个;分隔]
	private String receEmail;
	// 短信通知号码[多个;分隔]
	private String receSms;
	// 创建时间
	private Date createTime;

	//================================= 扩展属性
	// 系统名称
	private String sysName;
	// 事件名称
	private String eventName;
	
	public String getMsId() {
		return msId;
	}
	public void setMsId(String msId) {
		this.msId = msId;
	}
	
	public String getSysNo() {
		return sysNo;
	}
	public void setSysNo(String sysNo) {
		this.sysNo = sysNo;
	}
	
	public String getMeId() {
		return meId;
	}
	public void setMeId(String meId) {
		this.meId = meId;
	}
	
	public String getSubUserId() {
		return subUserId;
	}
	public void setSubUserId(String subUserId) {
		this.subUserId = subUserId;
	}
	
	public String getReceEmail() {
		return receEmail;
	}
	public void setReceEmail(String receEmail) {
		this.receEmail = receEmail;
	}
	
	public String getReceSms() {
		return receSms;
	}
	public void setReceSms(String receSms) {
		this.receSms = receSms;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getSysName() {
		return sysName;
	}
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
}
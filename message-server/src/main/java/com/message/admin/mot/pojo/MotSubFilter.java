package com.message.admin.mot.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * mot_sub_filter实体
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Alias("motSubFilter")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MotSubFilter extends BaseEntity implements Serializable {
	// 编号
	private String msfId;
	// 事件订阅人编号
	private String msId;
	// 类型
	private String type;
	// 字段1
	private String field1;
	// 创建时间
	private Date createTime;
	// 字段2
	private String field2;
	// 字段3
	private String field3;
	// 字段4
	private String field4;
	// 字段5
	private Integer field5;
	// 字段6
	private Double field6;
	// 字段7
	private String field7;
	// 字段8
	private String field8;
	
	public String getMsfId() {
		return msfId;
	}
	public void setMsfId(String msfId) {
		this.msfId = msfId;
	}
	
	public String getMsId() {
		return msId;
	}
	public void setMsId(String msId) {
		this.msId = msId;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	
	public Integer getField5() {
		return field5;
	}
	public void setField5(Integer field5) {
		this.field5 = field5;
	}
	
	public Double getField6() {
		return field6;
	}
	public void setField6(Double field6) {
		this.field6 = field6;
	}
	
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
}
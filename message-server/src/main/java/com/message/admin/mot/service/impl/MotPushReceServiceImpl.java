package com.message.admin.mot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.mot.dao.MotPushReceDao;
import com.message.admin.mot.pojo.MotPushRece;
import com.message.admin.mot.pojo.MotSub;
import com.message.admin.mot.service.MotPushReceService;
import com.message.admin.mot.service.MotSubService;
import com.system.comm.model.Page;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_push_rece的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public class MotPushReceServiceImpl implements MotPushReceService {

	@Autowired
	private MotPushReceDao motPushReceDao;
	@Autowired
	private MotSubService motSubService;
	
	@Override
	public ResponseFrame save(MotPushRece motPushRece) {
		ResponseFrame frame = new ResponseFrame();
		motPushReceDao.save(motPushRece);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MotPushRece get(String mpId, String msId) {
		return motPushReceDao.get(mpId, msId);
	}

	@Override
	public ResponseFrame pageQuery(MotPushRece motPushRece) {
		motPushRece.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = motPushReceDao.findMotPushReceCount(motPushRece);
		List<MotPushRece> data = null;
		if(total > 0) {
			data = motPushReceDao.findMotPushRece(motPushRece);
		}
		Page<MotPushRece> page = new Page<MotPushRece>(motPushRece.getPage(), motPushRece.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String mpId) {
		ResponseFrame frame = new ResponseFrame();
		motPushReceDao.delete(mpId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<MotPushRece> findByMpId(String mpId) {
		List<MotPushRece> list = motPushReceDao.findByMpId(mpId);
		for (MotPushRece mpr : list) {
			MotSub ms = motSubService.get(mpr.getMsId());
			mpr.setReceUserId(ms.getSubUserId());
			mpr.setReceEmail(ms.getReceEmail());
			mpr.setReceSms(ms.getReceSms());
		}
		return list;
	}
}
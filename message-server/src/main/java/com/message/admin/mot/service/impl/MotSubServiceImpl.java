package com.message.admin.mot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.mot.dao.MotSubDao;
import com.message.admin.mot.pojo.MotSub;
import com.message.admin.mot.service.MotEventService;
import com.message.admin.mot.service.MotSubFilterService;
import com.message.admin.mot.service.MotSubService;
import com.message.admin.sys.service.SysInfoService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_sub的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public class MotSubServiceImpl implements MotSubService {

	@Autowired
	private MotSubDao motSubDao;
	@Autowired
	private SysInfoService sysInfoService;
	@Autowired
	private MotEventService motEventService;
	@Autowired
	private MotSubFilterService motSubFilterService;
	
	@Override
	public ResponseFrame saveOrUpdate(MotSub motSub) {
		ResponseFrame frame = new ResponseFrame();
		MotSub org = getByMeIdSubuid(motSub.getMeId(), motSub.getSubUserId());
		if(org == null) {
			motSub.setMsId(FrameNoUtil.uuid());
			motSubDao.save(motSub);
		} else {
			motSub.setMsId(org.getMsId());
			motSubDao.update(motSub);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MotSub get(String msId) {
		return motSubDao.get(msId);
	}

	@Override
	public ResponseFrame pageQuery(MotSub motSub) {
		motSub.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = motSubDao.findMotSubCount(motSub);
		List<MotSub> data = null;
		if(total > 0) {
			data = motSubDao.findMotSub(motSub);
			for (MotSub ms : data) {
				ms.setSysName(sysInfoService.getName(ms.getSysNo()));
				ms.setEventName(motEventService.getName(ms.getMeId()));
			}
		}
		Page<MotSub> page = new Page<MotSub>(motSub.getPage(), motSub.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String msId) {
		ResponseFrame frame = new ResponseFrame();
		motSubDao.delete(msId);
		motSubFilterService.deleteByMsId(msId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<MotSub> findByMeId(String meId) {
		return motSubDao.findByMeId(meId);
	}

	@Override
	public MotSub getByMeIdSubuid(String meId, String subUserId) {
		return motSubDao.getByMeIdSubuid(meId, subUserId);
	}
}
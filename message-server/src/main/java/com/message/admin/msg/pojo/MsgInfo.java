package com.message.admin.msg.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * msg_info实体
 * @author autoCode
 * @date 2017-12-04 17:13:15
 * @version V1.0.0
 */
@Alias("msgInfo")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class MsgInfo extends BaseEntity implements Serializable {
	//编号
	private String id;
	//消息分组编号
	private String groupId;
	//标题
	private String title;
	//内容
	private String content;
	//创建时间
	private Date createTime;
	//发送人
	private String sendUserId;
	//状态[10待发送、20已发送]
	private Integer status;
	//发送时间
	private Date sendTime;
	//类型[10阅读、20待办]
	private Integer type;
	//扩展1
	private String ext1;
	//扩展2
	private String ext2;
	//扩展3
	private String ext3;
	
	//============================== 扩展属性
	//查询使用的用户编号
	private String userId;
	//系统编码
	private String sysNo;
	//接收人编码
	private String receUserId;
	//接收人编码集合字符串
	private String receUserIds;
	//查询使用的是否已读
	private Integer isRead;
	//阅读时间
	private Date readTime;
	//分组编码集合字符串
	private String groupIds;
	//分组编码集合
	private List<String> groupIdList;
	//接收短信或邮件的内容[不传默认为content内容]
	private String receContent;
	//接收短信的内容[不传默认为receContent内容]
	private String smsContent;
	//邮件的附件[{"name":"test.txt","path":"http://xx.xx.x/dafsdf.txt"}]
	private String receEmailFiles;
	//接收手机[多个;分隔]
	private String recePhones;
	//接收邮箱[多个;分隔]
	private String receEmails;
	//类型名称
	private String typeName;
	//是否打开邮件短信校验[0否、1是]
	private Integer isOpenCheck;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getSendUserId() {
		return sendUserId;
	}
	public void setSendUserId(String sendUserId) {
		this.sendUserId = sendUserId;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public String getReceUserIds() {
		return receUserIds;
	}
	public void setReceUserIds(String receUserIds) {
		this.receUserIds = receUserIds;
	}
	public String getSysNo() {
		return sysNo;
	}
	public void setSysNo(String sysNo) {
		this.sysNo = sysNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getIsRead() {
		return isRead;
	}
	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}
	public Date getReadTime() {
		return readTime;
	}
	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}
	public String getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}
	public List<String> getGroupIdList() {
		return groupIdList;
	}
	public void setGroupIdList(List<String> groupIdList) {
		this.groupIdList = groupIdList;
	}
	public String getReceContent() {
		return receContent;
	}
	public void setReceContent(String receContent) {
		this.receContent = receContent;
	}
	public String getRecePhones() {
		return recePhones;
	}
	public void setRecePhones(String recePhones) {
		this.recePhones = recePhones;
	}
	public String getReceEmails() {
		return receEmails;
	}
	public void setReceEmails(String receEmails) {
		this.receEmails = receEmails;
	}
	public String getReceEmailFiles() {
		return receEmailFiles;
	}
	public void setReceEmailFiles(String receEmailFiles) {
		this.receEmailFiles = receEmailFiles;
	}
	public String getReceUserId() {
		return receUserId;
	}
	public void setReceUserId(String receUserId) {
		this.receUserId = receUserId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getExt2() {
		return ext2;
	}
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	public String getExt3() {
		return ext3;
	}
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	public Integer getIsOpenCheck() {
		return isOpenCheck;
	}
	public void setIsOpenCheck(Integer isOpenCheck) {
		this.isOpenCheck = isOpenCheck;
	}
	public String getSmsContent() {
		return smsContent;
	}
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}
}
package com.message.admin.send.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * send_email实体
 * @author autoCode
 * @date 2017-12-13 11:15:57
 * @version V1.0.0
 */
@Alias("sendEmail")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class SendEmail extends BaseEntity implements Serializable {
	//编号
	private String id;
	//消息编号
	private String msgId;
	//接收邮箱
	private String email;
	//邮件标题
	private String title;
	//发送内容
	private String content;
	//发送时间
	private Date sendTime;
	//状态[10待发送、20发送中、30发送成功、40发送失败]
	private Integer status;
	//创建时间
	private Date createTime;
	//处理服务的唯一编码
	private String servNo;
	//附件集合[{"name":"test.txt","path":"http://xx.xx.x/dafsdf.txt"}]
	private String files;
	
	//========================= 扩展属性
	//状态名称
	private String statusName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getServNo() {
		return servNo;
	}
	public void setServNo(String servNo) {
		this.servNo = servNo;
	}
	/**
	 * 附件集合[{"name":"test.txt","path":"http://xx.xx.x/dafsdf.txt"}]
	 * @return
	 */
	public String getFiles() {
		return files;
	}
	public void setFiles(String files) {
		this.files = files;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
}
package com.message.admin.send.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.send.dao.SendEmailDao;
import com.message.admin.send.enums.SendStatus;
import com.message.admin.send.pojo.SendEmail;
import com.message.admin.send.service.SendEmailService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameMapUtil;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 发送邮件的Service
 * @author autoCode
 * @date 2017-12-13 11:15:57
 * @version V1.0.0
 */
@Component
public class SendEmailServiceImpl implements SendEmailService {

	@Autowired
	private SendEmailDao sendEmailDao;
	
	@Override
	public ResponseFrame saveList(String msgId, String title, String content, String emails,
			Date sendTime, String emailFiles) {
		ResponseFrame frame = new ResponseFrame();
		if(FrameStringUtil.isEmpty(title) || FrameStringUtil.isEmpty(content)) {
			frame.setCode(-2);
			frame.setMessage("发送内容不能为空");
			return frame;
		}
		if(FrameStringUtil.isEmpty(emails)) {
			frame.setCode(-2);
			frame.setMessage("不存在接收邮箱");
			return frame;
		}
		List<String> emailList = FrameStringUtil.toArray(emails, ";");
		if(emailList.size() == 0) {
			frame.setCode(-2);
			frame.setMessage("不存在接收邮箱");
			return frame;
		}
		if(FrameStringUtil.isEmpty(title)) {
			if(content.length() > 140) {
				title = content.substring(0, 140);
			} else {
				title = content;
			}
		}
		Map<String, String> sendMap = new HashMap<String, String>();
		for (String email : emailList) {
			if(FrameStringUtil.isEmpty(email)) {
				continue;
			}
			if(FrameStringUtil.isNotEmpty(FrameMapUtil.getString(sendMap, email))) {
				//不为空证明已经发送过了
				continue;
			}
			SendEmail sendEmail = new SendEmail();
			sendEmail.setMsgId(msgId);
			sendEmail.setTitle(title);
			sendEmail.setContent(content);
			sendEmail.setEmail(email);
			sendEmail.setSendTime(sendTime);
			sendEmail.setFiles(emailFiles);
			save(sendEmail);
			sendMap.put(email, email);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	private void save(SendEmail sendEmail) {
		SendEmail org = sendEmailDao.getMsgIdEmail(sendEmail.getMsgId(), sendEmail.getEmail());
		if(org != null) {
			//存在相同的不入库
			return;
		}
		sendEmail.setId(FrameNoUtil.uuid());
		if(sendEmail.getStatus() == null) {
			sendEmail.setStatus(SendStatus.WAIT.getCode());
		}
		sendEmail.setCreateTime(FrameTimeUtil.getTime());
		sendEmailDao.save(sendEmail);
	}

	@Override
	public SendEmail get(String id) {
		return sendEmailDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(SendEmail sendEmail) {
		sendEmail.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = sendEmailDao.findSendEmailCount(sendEmail);
		List<SendEmail> data = null;
		if(total > 0) {
			data = sendEmailDao.findSendEmail(sendEmail);
			for (SendEmail se : data) {
				se.setStatusName(SendStatus.getText(se.getStatus()));
			}
		}
		Page<SendEmail> page = new Page<SendEmail>(sendEmail.getPage(), sendEmail.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String id) {
		ResponseFrame frame = new ResponseFrame();
		sendEmailDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<SendEmail> findIng(String servNo) {
		return sendEmailDao.findIng(servNo);
	}

	@Override
	public void updateWaitToIng(String servNo, Integer num) {
		sendEmailDao.updateWaitToIng(servNo, num);
	}

	@Override
	public void updateStatus(String id, Integer status) {
		sendEmailDao.updateStatus(id, status);
	}
}
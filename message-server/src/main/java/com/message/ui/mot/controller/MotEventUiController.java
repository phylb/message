package com.message.ui.mot.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.mot.pojo.MotEvent;
import com.message.admin.mot.service.MotEventService;
import com.message.admin.mot.utils.MotPluginUtil;
import com.message.admin.sys.service.SysInfoService;
import com.message.ui.comm.controller.BaseController;
import com.message.ui.sys.pojo.AdminUser;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_event的Controller
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Controller
public class MotEventUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MotEventUiController.class);

	@Autowired
	private MotEventService motEventService;
	@Autowired
	private SysInfoService sysInfoService;
	
	@RequestMapping(value = "/motEvent/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/mot/event-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/motEvent/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response, MotEvent motEvent) {
		ResponseFrame frame = null;
		try {
			frame = motEventService.pageQuery(motEvent);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/motEvent/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, String meId) {
		if(FrameStringUtil.isNotEmpty(meId)) {
			MotEvent motEvent = motEventService.get(meId);
			modelMap.put("motEvent", motEvent);
		}
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		modelMap.put("plugins", MotPluginUtil.getPlugins());
		return "admin/mot/event-edit";
	}
	
	@RequestMapping(value = "/motEvent/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response, MotEvent motEvent) {
		ResponseFrame frame = null;
		try {
			AdminUser user = getSessionSysUser(request);
			motEvent.setCreateUserId(user.getId());
			frame = motEventService.saveOrUpdate(motEvent);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/motEvent/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String meId) {
		ResponseFrame frame = null;
		try {
			frame = motEventService.delete(meId);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	@RequestMapping(value = "/motEvent/f-json/findBySysNo")
	@ResponseBody
	public void findBySysNo(HttpServletRequest request, HttpServletResponse response, String sysNo) {
		ResponseFrame frame = new ResponseFrame();
		try {
			List<MotEvent> data = motEventService.findBySysNo(sysNo);
			frame.setBody(data);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}
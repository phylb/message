package com.message.ui.mot.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.mot.pojo.MotSubFilter;
import com.message.admin.mot.service.MotSubFilterService;
import com.message.ui.comm.controller.BaseController;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_sub_filter的Controller
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Controller
public class MotSubFilterUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MotSubFilterUiController.class);

	@Autowired
	private MotSubFilterService motSubFilterService;
	
	@RequestMapping(value = "/motSubFilter/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		return "admin/mot/subFilter-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/motSubFilter/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response, MotSubFilter motSubFilter) {
		ResponseFrame frame = null;
		try {
			frame = motSubFilterService.pageQuery(motSubFilter);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/motSubFilter/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, String msfId) {
		if(FrameStringUtil.isNotEmpty(msfId)) {
			MotSubFilter motSubFilter = motSubFilterService.get(msfId);
			modelMap.put("motSubFilter", motSubFilter);
		}
		return "admin/mot/subFilter-edit";
	}
	
	@RequestMapping(value = "/motSubFilter/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response, MotSubFilter motSubFilter) {
		ResponseFrame frame = null;
		try {
			frame = motSubFilterService.saveOrUpdate(motSubFilter);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/motSubFilter/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String msfId) {
		ResponseFrame frame = null;
		try {
			frame = motSubFilterService.delete(msfId);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}
package com.message.ui.msg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.send.pojo.SendEmail;
import com.message.admin.send.service.SendEmailService;
import com.message.ui.comm.controller.BaseController;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 发送的Email的Controller
 * @author yuejing
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class SendEmailUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailUiController.class);

	@Autowired
	private SendEmailService sendEmailService;

	@RequestMapping(value = "/sendEmail/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		return "admin/msg/sendEmail-manager";
	}
	
	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/sendEmail/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			SendEmail sendEmail) {
		ResponseFrame frame = null;
		try {
			frame = sendEmailService.pageQuery(sendEmail);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/sendEmail/f-json/updateStatus")
	@ResponseBody
	public void updateStatus(HttpServletRequest request, HttpServletResponse response, String id, Integer status) {
		ResponseFrame frame = new ResponseFrame();
		try {
			sendEmailService.updateStatus(id, status);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/sendEmail/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		ResponseFrame frame = null;
		try {
			frame = sendEmailService.delete(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}
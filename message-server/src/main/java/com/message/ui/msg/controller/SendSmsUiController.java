package com.message.ui.msg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.send.pojo.SendSms;
import com.message.admin.send.service.SendSmsService;
import com.message.ui.comm.controller.BaseController;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 发送的Sms的Controller
 * @author yuejing
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class SendSmsUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendSmsUiController.class);

	@Autowired
	private SendSmsService sendSmsService;

	@RequestMapping(value = "/sendSms/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		return "admin/msg/sendSms-manager";
	}
	
	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/sendSms/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			SendSms sendSms) {
		ResponseFrame frame = null;
		try {
			frame = sendSmsService.pageQuery(sendSms);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
		}
		writerJson(response, frame);
	}


	@RequestMapping(value = "/sendSms/f-json/updateStatus")
	@ResponseBody
	public void updateStatus(HttpServletRequest request, HttpServletResponse response, String id, Integer status) {
		ResponseFrame frame = new ResponseFrame();
		try {
			sendSmsService.updateStatus(id, status);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/sendSms/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		ResponseFrame frame = null;
		try {
			frame = sendSmsService.delete(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}
package com.message.ui.sys.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * admin_user实体
 * @author autoCode
 * @date 2017-12-27 15:24:05
 * @version V1.0.0
 */
@Alias("adminUser")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class AdminUser extends BaseEntity implements Serializable {
	//编号
	private String id;
	//用户名
	private String username;
	//密码
	private String password;
	//创建时间
	private Date createTime;
	
	public AdminUser() {
		super();
	}
	public AdminUser(String id, String username, String password, Date createTime) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.createTime = createTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
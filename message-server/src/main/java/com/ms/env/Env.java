package com.ms.env;


/**
 * 配置文件的key
 * @author yuejing
 * @date 2016年5月16日 下午5:52:03
 * @version V1.0.0
 */
public enum Env {
	PROJECT_MODEL		("project.model", "项目模式[dev开发、test测试、release正式]"),
	PROJECT_NAME		("project.name", "项目名称"),
	CLIENT_AUTH_INFO	("client.auth.info", "客户端鉴权信息"),
	
	SEND_EMAIL_SMTP		("send.email.smtp", "发送邮箱的smtp"),
	SEND_EMAIL_FROM		("send.email.from", "发送邮箱的from"),
	SEND_EMAIL_USERNAME	("send.email.username", "发送邮箱的用户名"),
	SEND_EMAIL_PASSWORD	("send.email.password", "发送邮箱的密码"),
	
	PROJECT_DB_UPDATE	("project.db.update", "执行更新脚本[0否、1是，默认为1]"),
	;
	private String code;
	private String name;

	private Env(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
}
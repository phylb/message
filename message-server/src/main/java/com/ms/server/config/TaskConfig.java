package com.ms.server.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.message.admin.send.task.SendEmailTask;
import com.message.admin.send.task.SendSmsTask;

@Configuration
public class TaskConfig {

	/*@Autowired
	private Environment env;*/

	@Bean
	public SendSmsTask sendSmsTask() {
		//发送任务添加到redis消息队列中
		SendSmsTask sendSmsTask = new SendSmsTask();
		sendSmsTask.run(10, 5);
		return sendSmsTask;
	}
	
	@Bean
	public SendEmailTask sendEmailTask() {
		//发送任务添加到redis消息队列中
		SendEmailTask sendEmailTask = new SendEmailTask();
		sendEmailTask.run(15, 10);
		return sendEmailTask;
	}
}
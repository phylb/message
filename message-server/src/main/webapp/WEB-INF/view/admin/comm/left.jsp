<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld" %>
<div class="c-left left-menu">
	<div class="panel-group">
		<div class="panel panel-info top">
			<div class="panel-heading">
				<h4 class="panel-title text-center">
					<a href="${webroot}/adminUser/f-view/main"><span class="glyphicon glyphicon-home"></span> 个人中心</a>
				</h4>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'sysInfo'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapseSysInfo"><span class="glyphicon glyphicon-tasks"></span> 接入系统 <span class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapseSysInfo" class="panel-collapse collapse <c:if test="${param.first == 'sysInfo'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'sysInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/sysInfo/f-view/manager">系统管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'msgGroupManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/msgGroup/f-view/manager">系统分组</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'userInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/userInfo/f-view/manager">系统用户</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'mot'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapseMot"><span class="glyphicon glyphicon-dashboard"></span> MOT <span class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapseMot" class="panel-collapse collapse <c:if test="${param.first == 'mot'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'motEventManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/motEvent/f-view/manager">事件管理</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'motSubManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/motSub/f-view/manager">事件订阅</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'motPushManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/motPush/f-view/manager">推送事件</a>
					</div>
					<%-- 
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'userInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/userInfo/f-view/manager">系统用户</a>
					</div> --%>
				</div>
			</div>
		</div>
		<div class="panel panel-success center">
			<div class="panel-heading <c:if test="${param.first == 'msg'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapsePrj"><span class="glyphicon glyphicon-envelope"></span> 消息管理 <span
						class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapsePrj" class="panel-collapse collapse <c:if test="${param.first == 'msg'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'msgInfoManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/msgInfo/f-view/manager">消息记录</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'sendEmailManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/sendEmail/f-view/manager">邮件记录</a>
					</div>
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'sendSmsManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block" href="${webroot}/sendSms/f-view/manager">短信记录</a>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default bottom">
			<div class="panel-heading <c:if test="${param.first == 'sys'}">panel-select</c:if>">
				<h4 class="panel-title text-center">
					<a data-toggle="collapse" href="#mlCollapseSys"><span class="glyphicon glyphicon-cog"></span> 系统设置 <span
						class="caret"></span></a>
				</h4>
			</div>
			<div id="mlCollapseSys" class="panel-collapse collapse <c:if test="${param.first == 'sys'}">in</c:if>">
				<div class="panel-body">
					<div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'adminUserManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block"
							href="${webroot}/adminUser/f-view/manager">管理员管理</a>
					</div>
					<%-- <div>
						<a class="btn btn-<c:choose><c:when test="${param.second == 'sysConfigManager'}">select</c:when><c:otherwise>link</c:otherwise></c:choose> btn-block"
							href="${webroot}/sysConfig/f-view/manager.shtml">系统配置</a>
					</div> --%>
				</div>
			</div>
		</div>
	</div>
</div>
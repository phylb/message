<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-推送事件</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-sm">
  		<div class="form-group">
			<label for="sysNo" class="col-sm-4">系统编码 <span class="text-danger">*</span></label>
			<div class="col-sm-8">${motPush.sysName}</div>
		</div>
		<div class="form-group">
			<label for="sysNo" class="col-sm-4">订阅事件 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				${motPush.eventName}
			</div>
		</div>
  		<div class="form-group">
			<label for="title" class="col-sm-4">推送标题 <span class="text-danger">*</span></label>
			<div class="col-sm-8">${motPush.title}</div>
		</div>
  		<div class="form-group">
			<label for="content" class="col-sm-4">推送内容</label>
			<div class="col-sm-8">${motPush.content}</div>
		</div>
  		<div class="form-group">
			<label for="receUserIds" class="col-sm-4">接收人 <span class="text-danger">*</span></label>
			<div class="col-sm-8">${motPush.receUserIds}</div>
		</div>
  		<div class="form-group">
			<label for="emailContent" class="col-sm-4">邮件内容</label>
			<div class="col-sm-8">${motPush.emailContent}</div>
		</div>
  		<div class="form-group">
			<label for="emailFiles" class="col-sm-4">邮件附件</label>
			<div class="col-sm-8">${motPush.emailFiles}</div>
		</div>
  		<div class="form-group">
			<label for="smsContent" class="col-sm-4">短信内容</label>
			<div class="col-sm-8">${motPush.smsContent}</div>
		</div>
		
		<div class="form-table">
			<table class="table" id="dtlTable">
				<thead>
				<tr class="info">
					<th>类型 <span class="text-danger">*</span></th>
					<th>字段1 <span class="text-danger">*</span></th>
					<th>字段2</th>
					<th>字段3</th>
					<th>字段4</th>
					<th>字段5</th>
					<th>字段6</th>
					<th>字段7</th>
					<th>字段8</th>
				</tr>
				</thead>
				<tbody id="dtlTableTbody">
				<c:forEach items="${motPush.filters}" var="item">
				<tr>
					<td><div class="form-group w-60">${item.type}</div>
					</td>
					<td><div class="form-group w-60">${item.field1}</div>
					</td>
					<td><div class="form-group w-60">${item.field2}</div>
					</td>
					<td><div class="form-group w-60">${item.field3}</div>
					</td>
					<td><div class="form-group w-60">${item.field4}</div>
					</td>
					<td><div class="form-group w-60">${item.field5}</div>
					</td>
					<td><div class="form-group w-60">${item.field6}</div>
					</td>
					<td><div class="form-group w-60">${item.field7}</div>
					</td>
					<td><div class="form-group w-60">${item.field8}</div>
					</td>
				</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
		
		
		<div class="form-table">
			<table class="table" id="receDtlTable">
				<thead>
				<tr class="success">
					<th width="100">实际接收人</th>
					<th>接收邮箱</th>
					<th>接收短信的号码</th>
				</tr>
				</thead>
				<tbody id="dtlSubTableTbody">
				<c:forEach items="${motPush.reces}" var="item">
				<tr>
					<td><div class="form-group">${item.receUserId}</div>
					</td>
					<td><div class="form-group">${item.receEmail}</div>
					</td>
					<td><div class="form-group">${item.receSms}</div>
					</td>
				</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
		
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" onclick="parent.dialog.close()" class="btn btn-default enter-fn">关闭</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
	});
	</script>
</body>
</html>
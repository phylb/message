<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-邮件记录</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp">
			<jsp:param name="first" value="msg"/>
			<jsp:param name="second" value="sendEmailManager"/>
		</jsp:include>
		<div class="c-right">
			<div class="panel panel-success">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">消息管理 / <b>邮件记录</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
				  	<div class="table-tool-panel">
						<div class="row">
							<div class="col-sm-6 enter-panel">
								<div class="btn-group">
									<my:select dictcode="send_status" id="status" headerKey="" headerValue="所有发送状态" cssCls="form-control input-sm" exp="onchange=\"info.loadInfo(1)\""/>
								</div>
								<!-- <input type="text" style="width: 150px;display: inline;" class="form-control input-sm" id="receUserId" placeholder="接收人编码[精确查找]" value=""> -->
							  	<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
							</div>
							<div class="col-sm-6 text-right">
							  	<!-- <div class="btn-group">
							  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增密钥</a>
							  	</div> -->
							</div>
						</div>
				  	</div>
					<div id="infoPanel" class="table-panel"></div>
					<div id="infoPage" class="table-page-panel"></div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                         '<th width="200">接收源</th>',
				                         '<th>内容</th>',
				                         '<th width="150">创建时间</th>',
				                         '<th width="120">发送状态</th>',
				                         '<th width="150">发送时间</th>',
				                         '<th width="80">操作</th>',
				                         '</tr></thead><tbody>'].join('');
				infoPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				infoPage.page = page;
			
			JUtil.ajax({
				url : '${webroot}/sendEmail/f-json/pageQuery',
				data : { page:infoPage.page, size:infoPage.size, status: $('#status').val() },
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							var cls = '';
							if(obj.status == 30)
								cls = 'label label-success';
							else if(obj.status == 40)
								cls = 'label label-danger';
							else
								cls = 'label label-default';
							return ['<tr>',
							    	'<td>',obj.email,'</td>',
							    	'<td>',obj.content,'</td>',
							    	'<td>',obj.createTime,'</td>',
							    	'<td><span class="',cls,'">',obj.statusName,'</span></td>',
							    	'<td>',obj.sendTime,'</td>',
							    	'<td>',
							    	'<a class="glyphicon text-success" href="javascript:info.reset(\'',obj.id,'\')" title="重发">重发</a>',
							    	'&nbsp; <a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(\'',obj.id,'\')" title="删除"></a>',
							    	'</td>',
								'</tr>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		change: function() {
			info.loadInfo(1);
		},
		reset : function(id) {
			if(confirm('您确定要重发吗?')) {
				JUtil.ajax({
					url : '${webroot}/sendEmail/f-json/updateStatus',
					data : { id: id, status:10 },
					success : function(json) {
						if (json.code === 0) {
							message('重发成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		},
		del : function(id) {
			if(confirm('您确定要删除吗?')) {
				JUtil.ajax({
					url : '${webroot}/sendEmail/f-json/delete',
					data : { id: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		}
};
$(function() {
	info.loadInfo(1);
});
</script>
</body>
</html>
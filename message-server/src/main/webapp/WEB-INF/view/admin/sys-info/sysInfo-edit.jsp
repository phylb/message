<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑系统</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
  		<div class="form-group">
			<label for="sysNo" class="col-sm-4">编码 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="sysNo" placeholder="编码" value="${sysInfo.sysNo}"></div>
		</div>
  		<div class="form-group">
			<label for="name" class="col-sm-4">名称 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="name" placeholder="名称" value="${sysInfo.name}"></div>
		</div>
  		<div class="form-group">
			<label for="clientId" class="col-sm-4">客户端编码</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="clientId" placeholder="客户端的编码" value="${sysInfo.clientId}"></div>
		</div>
  		<div class="form-group">
			<label for="token" class="col-sm-4">客户端密钥</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="token" placeholder="客户端对应的密码" value="${sysInfo.token}"></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			
			var _sysNo = $('#sysNo');
			if(JUtil.isEmpty(_sysNo.val())) {
				_saveMsg.append('请输入编码');
				_sysNo.focus();
				return;
			}
			var _name = $('#name');
			if(JUtil.isEmpty(_name.val())) {
				_saveMsg.append('请输入名称');
				_name.focus();
				return;
			}

			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/sysInfo/f-json/save',
				data : {
					sysNo: _sysNo.val(),
					name: _name.val(),
					clientId: $('#clientId').val(),
					token: $('#token').val()
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑用户分组规则</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
  		<div class="form-group">
			<label for="status" class="col-sm-4">接收状态 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<my:radio name="status" dictcode="user_group_rule_status" defvalue="10" value="${userGroupRule.status}"/>
			</div>
		</div>
  		<div class="form-group">
			<label for="smsStatus" class="col-sm-4">接收短信 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<my:radio name="smsStatus" dictcode="user_group_rule_status" defvalue="10" value="${userGroupRule.smsStatus}"/>
			</div>
		</div>
  		<div class="form-group">
			<label for="emailStatus" class="col-sm-4">接收邮件 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<my:radio name="emailStatus" dictcode="user_group_rule_status" defvalue="10" value="${userGroupRule.emailStatus}"/>
			</div>
		</div>
  		<div class="form-group">
			<label for="recePhone" class="col-sm-4">接收手机</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="recePhone" placeholder="接收手机[多个;分隔]" value="${userGroupRule.recePhone}"></div>
		</div>
  		<div class="form-group">
			<label for="receEmail" class="col-sm-4">接收邮箱</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="receEmail" placeholder="接收邮箱[多个;分隔]" value="${userGroupRule.receEmail}"></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();

			var _status = $('input[name="status"]:checked').val();
			var _smsStatus = $('input[name="smsStatus"]:checked').val();
			var _emailStatus = $('input[name="emailStatus"]:checked').val();

			if(_status == 20 && (_smsStatus == 10 || _emailStatus == 10)) {
				_saveMsg.append('接收状态为关闭 接收短信和接收邮箱不能为打开');
				return;
			}
			
			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/userGroupRule/f-json/save',
				data : {
					userId: '${param.userId}',
					sysNo: '${param.sysNo}',
					groupId: '${param.groupId}',
					status: _status,
					smsStatus: _smsStatus,
					emailStatus: _emailStatus,
					recePhone: $('#recePhone').val(),
					receEmail: $('#receEmail').val()
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>
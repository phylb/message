<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑用户</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
		<input type="hidden" id="id" value="${adminUser.id}">
  		<div class="form-group">
			<label for="username" class="col-sm-4">用户名 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="username" placeholder="用户名" value="${adminUser.username}"></div>
		</div>
  		<div class="form-group">
			<label for="password" class="col-sm-4">密码 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="password" placeholder="密码" value=""></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			
			var _id = $('#id').val();
			var _username = $('#username');
			if(JUtil.isEmpty(_username.val())) {
				_saveMsg.append('请输入用户名');
				_username.focus();
				return;
			}
			var _password = $('#password');
			if(_id === '' && JUtil.isEmpty(_password.val())) {
				_saveMsg.append('请输入密码');
				_password.focus();
				return;
			}

			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/adminUser/f-json/save',
				data : {
					id: _id,
					username: _username.val(),
					password: _password.val()
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>
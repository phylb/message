package com.message.redis;

import java.util.List;

import com.system.cache.redis.BaseCache;
import com.system.cache.redis.RedisClient;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameStringUtil;

public class RedisServerTest {

	public static void main(String[] args) {
		RedisClient redis = new RedisClient();
		String hostString = "127.0.0.1:6379;";
		List<String> hosts = FrameStringUtil.toArray(hostString, ";");
		RedisClient.setHosts(hosts);
		RedisClient.setPassword("");
		RedisClient.setMaxTotal(500);
		RedisClient.setMaxIdle(1000);
		RedisClient.setMaxWaitMillis(100000);
		RedisClient.setKeyPrefix("test");
		
		String key = "test";
		BaseCache cache = new BaseCache();
		cache.setRedisClient(redis);
		cache.lpush(key, "1");
		cache.lpush(key, "2");
		cache.lpush(key, "3");
		cache.lpush(key, "4");
		List<String> list = cache.lrange(key);
		System.out.println(FrameJsonUtil.toString(list));
	}
}
